FROM liliancal/ubuntu-php-apache
ADD src /var/www
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]